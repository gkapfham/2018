---
name: Morty
tier: startup
site_url: https://www.himorty.com/pygotham/
logo: morty.svg
---
Morty is the smart way to get a mortgage. We're building technology to bring education,
transparency, and savings to homebuyers everywhere.
