---
name: Noemi Derzsy
talks:
- Network/Graph Analysis in Python
---

Holding a PhD in Physics and research background in Network Science and Computer Science, my interests revolve around the study of complex systems and complex networks through real-world data. Currently, I am a Senior Inventive Scientist at AT&T Labs within the Data Science and AI Research organization, doing lots of science with lots of data. Previously, I was a Data Science Fellow at Insight Data Science NYC and a postdoctoral research associate at Social Cognitive Networks Academic Research Center at Rensselaer Polytechnic Institute.