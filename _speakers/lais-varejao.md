---
name: Lais Varejão
talks:
- "Critical Incidents: a guide for developers"
---
I’m a computer scientist from Recife, Brazil. I currently work as a
full-stack developer and project manager at Vinta Software. Feminist. Django
Girls supporter and organizer. I’m also a proud Recurse Center alum, an
educational retreat for programmers in New York. On my free time, I enjoy
coffee, books & creative stuff.
