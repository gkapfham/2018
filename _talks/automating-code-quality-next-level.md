---
abstract: "You are likely familiar with code quality tools such as flake8 and pylint.
  However, these tools won\u2019t catch all issues that you may care about.  I will
  be diving into the more advanced types of quality checks that you can use (or even
  write yourself!) to further automate your development workflow."
duration: 25
level: Intermediate
room: PennTop North
slot: 2018-10-05 15:15:00-04:00
speakers:
- Kyle Knapp
title: 'Automating Code Quality: Next Level'
type: talk
---

Checking Python code quality by hand can be both tough and tedious. Fortunately, there exist many code quality tools that let you offload these quality checks to a machine. You are likely familiar with many of the popular tools such as `flake8`, `pylint`, and `coverage`. And that’s good! These tools will catch many different code quality issues such as: not following Python standards, code smells, and potential bugs. However, these basic tools won’t catch all potential code quality issues that you care about.  For example, you may want to catch code standard issues outside of PEP 8, bugs related to incorrect type assumptions, and invalid documentation examples. So, does that mean you sit idly by and continue to catch these issues by hand? Of course not! You find ways to offload the quality checks onto a machine because it will be faster, more accurate, and more consistent than you.

Building on previous talks about automating code quality, I will be diving into the more advanced types of quality checks that you can use (or even write yourself!) to further automate your development workflow. Using examples from real-world open source projects, you will learn about:

* Usage of more advanced, lesser-known quality checkers such as `mypy`
* Writing custom checkers for existing code quality tools such as `pylint` and `flake8`
* Using and writing checkers for documentation quality tools
