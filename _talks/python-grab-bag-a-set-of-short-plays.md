---
abstract: "A frenetic combination of educational and entertaining segments, as chosen
  by the audience! They\u2019re all different - plays, monologues, physical demos,
  jokes, or tiny traditional conference talks - and the audience chooses which they
  want next from a menu of presentation titles."
duration: 45
level: All
room: PennTop South
slot: 2018-10-06 11:15:00-04:00
speakers:
- Jason Owen
- Sumana Harihareswara
title: 'Python Grab Bag: A Set of Short Plays'
type: talk
---

A frenetic combination of educational and entertaining segments, as chosen
by the audience! In between segments, audience members will shout out
numbers from a menu, and we’ll perform the selected segment: it may be a
short monologue, it may be a play, it may be a physical demo, or it may be a
tiny traditional conference talk.

Audience members should walk away with some additional understanding of the
history of Python, knowledge of some tools and libraries available in the
Python ecosystem, and some Python-related amusement.

This talk is inspired by [the Neo-Futurists’ Infinite
Wrench](http://www.nyneofuturists.org/), a creative and energetic piece of
theater.