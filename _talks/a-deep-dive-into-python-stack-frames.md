---
abstract: Learn how CPython implements the function call stack and provides useful
  tracing and how embedders can leverage this to extract useful performance and reliability
  reports from their software!
duration: 30
level: Intermediate
room: Madison
slot: 2018-10-05 13:30:00-04:00
speakers:
- Nikhil Marathe
title: A deep dive into Python stack frames
type: talk
---

This talk walks listeners through CPython's implementation of call stacks and how information is encoded in these stacks to aid execution and error recovery. Once we cover that, we will learn how the profiling tool pyflame leverages this. I will then show how we modified the Crashpad crash reporter to extract these stacks from native crashes in the wild to make engineers lives easier.

The CPython interpreter implements function call stacks using various structures that act as chains of linked lists.
Each interpreter maintains a list of thread states for each thread. Each thread maintains a list of frames that represent the python function calls performed by PyEval_EvalFrameEx. New frames are pushed onto the link list and python can just walk the linked list when an exception is thrown or when a stack trace is requested. We look into how information about the filename, function and current line number is encoded and how it can be retrieved.

Then we look at how the sampling profiler pyflame makes clever use of OS primitives to reverse engineer a running python process and profile it. Finally I show how we adopted these ideas at my current employer to extract Python stacks using the open source Crashpad crash reporter. This allows our developers to detect crashes caused on _remote user machines_ due to incorrect Python code, with accurate Python stack traces.